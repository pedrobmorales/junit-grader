package com.pbm.junitgrader;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

public class GradingListener extends RunListener

{
	private boolean testPassed = false;
	private int score = 0;
	private int total = 0;

	@Override
	public void testRunFinished(Result result) throws Exception {
		System.out.printf("Test Run Finished: Score: %s/%s%n", score, total);
	}

	@Override
	public void testStarted(Description description) throws Exception {
		testPassed = true;

	}

	@Override
	public void testFailure(Failure failure) throws Exception {
		testPassed = false;
	}

	public boolean didTestPass() {
		return testPassed;
	}

	public void setScore(int score, int total) {
		this.score = score;
		this.total = total;

	}
}
