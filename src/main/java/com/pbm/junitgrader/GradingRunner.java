package com.pbm.junitgrader;

import java.lang.annotation.Annotation;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Ignore;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import com.pbm.junitgrader.annotations.Gradeable;

/**
 * An extension of the JUnit default runner, that inspects every test. Only runs
 * the tests that are {@link Gradeable}
 * 
 * @author pedro_morales
 *
 */
public class GradingRunner extends BlockJUnit4ClassRunner {

	private GradingListener listener;

	private boolean runOnce;

	private AtomicInteger score;

	private AtomicInteger total;

	public GradingRunner(Class<?> klass) throws InitializationError {
		super(klass);
		listener = new GradingListener();
		runOnce = false;
		score = new AtomicInteger(0);
		total = new AtomicInteger();
	}

	@Override
	protected void runChild(FrameworkMethod method, RunNotifier notifier) {

		if (!runOnce) {
			notifier.addListener(listener);
			runOnce = true;
		}
		// TODO Auto-generated method stub
		Annotation annots[] = method.getAnnotations();

		for (Annotation annot : annots) {

			Class<?> annotationType = annot.annotationType();

			if (annotationType == Ignore.class) {
				break;
			}
			if (annotationType == Gradeable.class) {

				super.runChild(method, notifier);
				Gradeable grade = (Gradeable) annot;

				total.addAndGet(grade.value());
				boolean passed = listener.didTestPass();
				if (passed) {
					score.addAndGet(grade.value());
				} else {

				}
				break;
			}

		}

		// Since this method runs for every test, accumulate the current score
		// totals combination.  At the end, we will know the results.
		listener.setScore(score.get(), total.get());

	}

}
