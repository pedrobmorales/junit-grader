package com.pbm.junitgrader;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pbm.junitgrader.annotations.Gradeable;

@RunWith(GradingRunner.class)
public class CalculatorTest {
	
	private Calculator testObject;

	/**
	 * 
	 */
	@Before
	public void setup()
	{
		testObject = new Calculator();
		testObject.toString();
	}
	
	@Test
	@Gradeable(10)
	public void foo()
	{
		
	}
	
	@Test
	@Gradeable(5)
	public void failedTest()
	{
		throw new IllegalArgumentException("Just fail");
	}
	
	@Test
	public void notGradeable()
	{
		
	}
	
	@Test
	@Ignore
	public void shouldIgnoreMe()
	{
		
	}
	
	@Test
	@Ignore
	@Gradeable(5)
	public void shouldIgnoreMe2()
	{
		
	}
	
	@Test
	@Gradeable(210)
	public void assertMe()
	{
		assertTrue(false);
	}
	
	@After
	public void teardown()
	{
		
	}
}
